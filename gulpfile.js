'use strict';

var gulp = require('gulp'),
	jade = require('gulp-jade'),
	stylus = require('gulp-stylus'),
	minifyCSS = require('gulp-minify-css'),
	del = require('del'),
	connect = require('gulp-connect'),
	rename = require('gulp-rename'),
	changed = require('gulp-changed'),
	uglify = require('gulp-uglify'),
	opn = require('opn'),
	nib = require('nib'),
	imageMin = require('gulp-imagemin'),
	clean = require('gulp-clean');


/**
 * Clean build directory
 */
gulp.task('clean', function () {
    gulp.src('build/**/*', {read: false})
        .pipe(clean({force: true}));
     setTimeout(function(){
		console.log('********** Clean complete! *************');
	}, 1000);
});


/**
 * Livereload server
 */
gulp.task('connect', function() {
	connect.server({
		root: 'build',
		port: 8888,
		livereload: true
	});
	opn('http://localhost:8888');
});

var jade = require('gulp-jade');

/**
 * Optimize image
 */
gulp.task('image', function() {
	gulp.src('app/img/**/*')
		.pipe(imageMin())
		.pipe(gulp.dest('build/img'))
		.pipe(connect.reload());
});


/**
 * Favicon move
 */
gulp.task('favicon', function() {
	gulp.src('app/favicon/**/*')
		.pipe(gulp.dest('build/favicon'))
		.pipe(connect.reload());
});


/** Jade compiling */
gulp.task('jade', function() {
	gulp.src('app/jade/**/[^_]*.jade')
		.pipe(jade({
			pretty: true
		}))
		.on('error', console.log)
		.pipe(rename({dirname: ''}))
		.pipe(gulp.dest('build'))
		.pipe(connect.reload());
});


/** Stulus compiling */
gulp.task('stylus', function() {
	gulp.src('app/stylus/**/[^_]*.styl')
		.pipe(stylus({
			use: nib()
		}))
		.on('error', console.log)
		.pipe(gulp.dest('build/css'))
		.pipe(minifyCSS())
		.pipe(rename({suffix: ".min",}))
		.pipe(gulp.dest('build/css'))
		.pipe(connect.reload());
});

/** JavaScripts move */
gulp.task('js', function() {
	gulp.src('app/js/**/*.js')
		.pipe(gulp.dest('build/js'))
		.pipe(uglify())
		.pipe(rename({suffix: ".min",}))
		.pipe(gulp.dest('build/js'))
		.pipe(connect.reload());
});

/** Fonts move */
gulp.task('font', function() {
	gulp.src('app/font/**/*')
		.pipe(gulp.dest('build/font'))
		.pipe(connect.reload());
});


/** Watch to all changes */
gulp.task('watch', function() {
	gulp.watch('app/jade/**/*.jade', ['jade']);
	gulp.watch('app/stylus/**/*.styl', ['stylus']);
	gulp.watch('app/js/**/*.js', ['js']);
	gulp.watch('app/img/**/*', ['image']);
	gulp.watch('app/favicon/**/*', ['favicon']);
	setTimeout(function(){
		console.log('********** Watch start! ***************');
	}, 1000);
});


/** Build task */
gulp.task('build', ['js', 'jade', 'stylus', 'font', 'image', 'favicon'], function() {
	setTimeout(function(){
		console.log('********** Build complete! *************');
	}, 1000);
});

/** Default task */
gulp.task('default', ['stylus', 'jade', 'watch', 'connect']);