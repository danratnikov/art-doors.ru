(function($) {

    $(window).on('load', function() {
        setTimeout(function() {
            $('.preloader').fadeOut();
        }, 5)
        console.log('Поехали!');
    });


    $(document).ready(function() {


        if ($(window).scrollTop() > 0) {
            $('.header').addClass('header__green');
        }
        $(window).on('scroll', function(e) {
            if ($(window).scrollTop() > 0) {
                $('.header').addClass('header__green');
            } else {
                $('.header').removeClass('header__green');
            }

        });
        
        

        $('.header__nav-butt').on('click', function(e) {
            $('.header').addClass('header__green');
            $('.header__nav-list').toggleClass('header__nav-list__visible');
        });

        $('.top-block__go-link').on('click', function(e) {
            e.preventDefault();
            var firstSectionTop = $(e.target.hash).offset().top - $('header').height();
            $('html, body').animate({
                scrollTop: firstSectionTop
            }, 1000);
        });

        function initialize() {
            var mapCanvas = document.getElementById('map');
            var myLatLng = {
                lat: 55.607288,
                lng: 37.645975
            };
            var mapOptions = {
                center: new google.maps.LatLng(55.607288, 37.645975),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: false,
                zoomControl: true,
                scaleControl: true,
                scrollwheel: false
            }
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'ООО Лесомодуль',
                icon: '../img/map/marker.png'
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
        
        
        var $portfolioSlider = $('#portfolio-slider');
        
        
         $portfolioSlider.owlCarousel({
            items: 3,
            pagination: false,
            navRewind: false,
            autoPlay: true,
            margin: 10
        });
         
        $('#portfolio-slider-prev').on('click', function() {
            $portfolioSlider.trigger('owl.prev');
        });
        $('#portfolio-slider-next').on('click', function() {

            $portfolioSlider.trigger('owl.next');
        });

    });

		$('img').lazyload({effect : "fadeIn"});
})(jQuery);
